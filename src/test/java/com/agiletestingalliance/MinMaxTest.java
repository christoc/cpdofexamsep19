package com.agiletestingalliance;


import org.junit.Assert;
import org.junit.Test;

public class MinMaxTest {
	
	@Test
	public void runF1() {
		MinMax test = new MinMax();
		
		Assert.assertEquals("Ok", 2, test.getMaxNumber(1, 2));	
	}
	
	@Test
	public void runF2() {
		MinMax test = new MinMax();
		
		Assert.assertEquals("Ok", 2, test.getMaxNumber(2, 1));	
	}	
	
}
