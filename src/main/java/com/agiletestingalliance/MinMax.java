package com.agiletestingalliance;

public class MinMax {

    public int getMaxNumber(int numberA, int numberB) {
        if (numberB > numberA) {
            return numberB;
	}
        else {
            return numberA;
	}
    }

}
